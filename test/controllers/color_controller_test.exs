defmodule SA.ColorControllerTest do
  use SA.ConnCase

  alias SA.Color
  @valid_attrs %{name: "some content"}
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, color_path(conn, :index)
    assert json_response(conn, 200)["data"] == []
  end

  test "shows chosen resource", %{conn: conn} do
    color = Repo.insert! %Color{}
    conn = get conn, color_path(conn, :show, color)
    assert json_response(conn, 200)["data"] == %{"id" => color.id,
      "name" => color.name}
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, color_path(conn, :show, -1)
    end
  end

  test "creates and renders resource when data is valid", %{conn: conn} do
    conn = post conn, color_path(conn, :create), color: @valid_attrs
    assert json_response(conn, 201)["data"]["id"]
    assert Repo.get_by(Color, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, color_path(conn, :create), color: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "updates and renders chosen resource when data is valid", %{conn: conn} do
    color = Repo.insert! %Color{}
    conn = put conn, color_path(conn, :update, color), color: @valid_attrs
    assert json_response(conn, 200)["data"]["id"]
    assert Repo.get_by(Color, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    color = Repo.insert! %Color{}
    conn = put conn, color_path(conn, :update, color), color: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "deletes chosen resource", %{conn: conn} do
    color = Repo.insert! %Color{}
    conn = delete conn, color_path(conn, :delete, color)
    assert response(conn, 204)
    refute Repo.get(Color, color.id)
  end
end
