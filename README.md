# SA

To start your Phoenix app:

  * Clone the repo with `git clone git@bitbucket.org:disa1985/simple_api.git`
  * Change directory to cloned repo
  * Install dependencies with `mix deps.get`
  * Create PostgreSQL user with `CREATE USER elixir WITH CREATEDB password 'phoenix';`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Populate database with seed data `mix run priv/repo/seeds.exs`
  * Start Phoenix endpoint with `mix phoenix.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

# API

  * From browser: http://localhost:4000/api/colors
  * From terminal: curl http://localhost:4000/api/colors
