defmodule SA.PageController do
  use SA.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
