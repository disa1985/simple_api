defmodule SA.ColorController do
  use SA.Web, :controller

  alias SA.Color

  def index(conn, _params) do
    colors = Repo.all(Color)
    render(conn, "index.json", colors: colors)
  end

  def create(conn, %{"color" => color_params}) do
    changeset = Color.changeset(%Color{}, color_params)

    case Repo.insert(changeset) do
      {:ok, color} ->
        conn
        |> put_status(:created)
        |> put_resp_header("location", color_path(conn, :show, color))
        |> render("show.json", color: color)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(SA.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    color = Repo.get!(Color, id)
    render(conn, "show.json", color: color)
  end

  def update(conn, %{"id" => id, "color" => color_params}) do
    color = Repo.get!(Color, id)
    changeset = Color.changeset(color, color_params)

    case Repo.update(changeset) do
      {:ok, color} ->
        render(conn, "show.json", color: color)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(SA.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    color = Repo.get!(Color, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(color)

    send_resp(conn, :no_content, "")
  end
end
