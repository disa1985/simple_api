defmodule SA.ColorView do
  use SA.Web, :view

  def render("index.json", %{colors: colors}) do
    %{data: render_many(colors, SA.ColorView, "color.json")}
  end

  def render("show.json", %{color: color}) do
    %{data: render_one(color, SA.ColorView, "color.json")}
  end

  def render("color.json", %{color: color}) do
    %{id: color.id,
      name: color.name}
  end
end
