# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :simple_api,
  namespace: SA,
  ecto_repos: [SA.Repo]

# Configures the endpoint
config :simple_api, SA.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "5PTo6UNIbjNIWx1jO90Eqp1SPjaufr9aDmEN3HimP09TIKnXn6uMgP6hjvLMRuKg",
  render_errors: [view: SA.ErrorView, accepts: ~w(html json)],
  pubsub: [name: SA.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
