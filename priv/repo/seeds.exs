# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     SA.Repo.insert!(%SA.SomeModel{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias SA.Repo
alias SA.Color

for name <- ~w(white black green blue yellow pink brown aqua) do
  Repo.get_by(Color, name: name) || Repo.insert!(%Color{name: name})
end

